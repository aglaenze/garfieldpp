#!/bin/bash

make clean
cmake -DCMAKE_INSTALL_PREFIX=$GARFIELD_HOME/Install -DWITH_EXAMPLES=ON -DCMAKE_PREFIX_PATH=/Users/aglaenzer/Softwares/GEANT4/geant4-install $GARFIELD_HOME

make -j8
make install

